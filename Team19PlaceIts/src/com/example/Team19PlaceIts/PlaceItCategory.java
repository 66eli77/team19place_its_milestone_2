package com.example.Team19PlaceIts;

public class PlaceItCategory {
	private final String category;
	private boolean active;
	public PlaceItCategory(String name) {
		category = name;
		this.active = false;
	}

	@Override
	public boolean equals(Object o) {
		return category.equalsIgnoreCase(((PlaceItCategory) o).category);
	}

	@Override
	public String toString() {
		return category;
	}
	public void on() {
		active = true;
	}
	public void off() {
		active = false;
	}
	
	public boolean getStatus() {
		return active;
	}
	
}
